public class Swap {

public static void main(String[]args) {

int x = 10;
int y = 20;

Integer X = new Integer(10);
Integer Y = new Integer(20);

System.out.println("x = " + x);
System.out.println("y = " + y);

System.out.println("X = " + X);
System.out.println("Y = " + Y);

// actual parameters
swap(x, y);
swap(X, Y);

System.out.println("x = " + x);
System.out.println("y = " + y);

System.out.println("X = " + X);
System.out.println("Y = " + Y);

}

public static void swap(int a, int b) {

// formal parameters
int temp;
temp = a;
a = b;
b = temp;

}

public static void swap (Integer a, Integer b) {

Integer temp;
temp = new Integer(a);
a = new Integer(b);
b = temp;

}

}
