public class PrintStatusBar {

public static void main(String[] args) {

	// first command line argument
	String numberOfSymbols = args[0];
	int numberOfSymbolsCount = Integer.parseInt(numberOfSymbols);

	// second command line argument
	String printedSymbol = args[1];
	System.out.println();
	// for loop for printing status bar
	for(int i = 0; i < numberOfSymbolsCount; i++) {
	System.out.print(printedSymbol);
	}
	System.out.println();
	System.out.println();

}

}
