
//*******************************
//Honor Code: The work I am submitting is a result of my own thinking and efforts.
//Kadeem LaFargue & Dylan Bish
//CMPSC 111 Spring 2017
//Lab #5
//Date: 02 16 2017
//
//Purpose:
//*******************************
import java.util.Date; // needed for printing today's date
import java.util.Scanner;
public class WordHide
{
	//-------------------------
	//main method: program execution begins here
	//-------------------------
	public static void main(String[] args)
	{
		// Label output with name and date:
		System.out.println("Kadeem LaFargue & Dylan Bish\nLab 5\n" + new Date() + "\n");

		// Variable dictionary: all the phrases needed to complete this program
		int count;
		String phrase1;
		String phrase2;
		String phrase3;
		String phrase4;
		String phrase5;
		String phrase6;
		String phrase7;
		String phrase8;
		String phrase9;
		String phrase10;
		String phrase11;
		String phrase12;
		String phrase13;



		Scanner scan = new Scanner(System.in);

		System.out.println("Enter a word or phrase.");
		phrase1 = scan.nextLine();
		phrase2 = phrase1.substring(0,10);
		phrase2 = phrase2.toUpperCase(); //the above code takes the phrase, cuts it off at the 9th input then changes it to uppercase

		phrase3 = phrase2.substring(0,1);
		System.out.println(phrase3 + " J M O Q W B V T Y R S P O I M L Z X F");

		phrase4 = phrase2.substring(1,2);
		System.out.println(phrase4 + " Y U A V F Z N K O P T K A C B A G T A");

		phrase5 = phrase2.substring(2,3);
		System.out.println(phrase5 + " E O T Z A Q J F G B N M O P L A I T G");

		phrase6 = phrase2.substring(3,4);
		System.out.println(phrase6 + " G Z E T U F G Q A S B V X K L P A G T");

		phrase7 = phrase2.substring(4,5);
		System.out.println(phrase7 + " L M N T A F Z G V H Y U Q S D R V B H");

		phrase8 = phrase2.substring(5,6);
		System.out.println(phrase8 + " P T B V C F J K Q F L T U V B Q S Q G");

		phrase9 = phrase2.substring(6,7);
		System.out.println(phrase9 + " K T A G A S M A N T L O A P Q T S Z A");

		phrase10 = phrase2.substring(7,8);
		System.out.println(phrase10 + " B I G T A F T M A N T L P O M N T A F");

		phrase11 = phrase2.substring(8,9);
		System.out.println(phrase11 + " I M S O K T A F I T Z V B N T Y A L O");

		phrase12 = phrase2.substring(9,10);
		System.out.println(phrase12 + " A Z T F A J G A Z M S T I Y U Q S D F");

		System.out.println("A T A F G Y U J A Z M I P L M T J G Y A");
		System.out.println("I M N T J A H G F R A Z V B P L M N O T");
		System.out.println("J A X V B N T A S K T A Z V B U L T A O");
		System.out.println("T Y A S V B H T A S K E T A Z M O T P L");
		System.out.println("O A Z M T A L O T A P Z L T A M T Q S N");
		System.out.println("T Q S K T A Z M T L Q P T A L T R I T Q");
		System.out.println("T D A N T D Y U I A Z C M T A Q W E R L");
		System.out.println("L T A M Z T A N Q T S F G J T A L M N T");
		System.out.println("I Q S F Z X W E A J T A K T L M N O T P");
		System.out.println("T A Z M T A I O L M N T A Z L T O P Q W");
		//all of these creates the grid, with the first 10 characters being the phrase


	}
}
