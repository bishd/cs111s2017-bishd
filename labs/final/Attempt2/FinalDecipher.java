import java.util.*;

public class FinalDecipher {
	private int offset;
	private char[] array;
	
	public FinalDecipher(int o, char[] a){
		offset = o;
		array = a;
		decrypt();
	}

	private void decrypt() {
		System.out.println("The deciphered message is ");
		for(int i=0; i < array.length; i++){
		array[i] -= offset;
		}
		for(int i=0; i < array.length; i++){
		System.out.print(array[i]); 	
		}
		System.out.println();
		System.out.println("This is with an offset of " + offset);

		

	}
	
}
	
