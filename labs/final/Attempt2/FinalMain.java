
//*************************************
// Honor Code: The work I am submitting is a result of my own thinking and efforts.
// Dylan Bish
// CMPSC 111 Spring 2017
// Lab #
// Date: 1 May 2017
//
// Purpose: To finish the semester
//*************************************
import java.util.Date; // needed for printing today's date
import java.util.*;
public class FinalMain //replace xxx with actual file name
{
	//-------------
	//main method: program execution begins
	//-------------
	public static void main(String[] args)
	{
		//Label output with name and date:
		System.out.println("Dylan Bish & Zijun Zhou\nFinal\n" + new Date() + "/n");
		
		//Variable dictionary:
		System.out.println("Welcome to the ciphering program!\nPlease enter a message you would like to encrypt.\nIf you are decrypting a message, input the ciphered message you received to decrypt it, using the negative counterpart of your integer as the offset:");

		
		Scanner scan = new Scanner(System.in);

		String message = scan.next();
		char[] array = message.toCharArray();
		System.out.println("The message you entered was:");
		for(int i=0; i < array.length; i++){
		System.out.print(array[i]); 	
		}
	System.out.println();

		FinalCipher cipher = new FinalCipher(3, array);
		FinalDecipher decipher = new FinalDecipher(3, array);
	}
}

