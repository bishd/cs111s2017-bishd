//=================================================
//// Honor Code: The work I am submitting is a result of my own thinking and efforts.
// Dylan Bish
// CMPSC 111 Spring 2017
// Lab 4
// Date: 9 February 2017
//
// Purpose: making some great Java art!
//=================================================
import java.awt.*;
import javax.swing.JApplet;

public class Lab4 extends JApplet
{
  //-------------------------------------------------
  // Use Graphics methods to add content to the drawing canvas
  //-------------------------------------------------
	public void paint(Graphics page)
	{
	  final int POWER = 275;
	  final int BOTTOM = 200; //these serve as starting points from various parts of the monitor

	  setBackground(Color.magenta);

	  page.setColor(Color.darkGray);
	  page.fillRect(200, 50, 200, 150); //plastic frame

	  page.setColor(Color.gray);
	  page.fillRect(BOTTOM+25, 75, BOTTOM-50, BOTTOM-100); //gray screen

	  page.setColor(Color.black);
	  page.fillRect(POWER, BOTTOM, 50, 20); //power frame and stand

	  page.setColor(Color.lightGray);
	  page.drawLine(POWER+5, BOTTOM+5, POWER+10, BOTTOM+5); //some buttons
	  page.drawLine(POWER+20, BOTTOM+5, POWER+25, BOTTOM+5);
	  page.drawLine(POWER+35, BOTTOM+5, POWER+40, BOTTOM+5);

	page.setColor(Color.lightGray);
	page.fillOval(POWER-25, POWER-60, 100, 10); //the base
	}
}
