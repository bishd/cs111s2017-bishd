//==========================================
// Honor Code: The work I am submitting is a result of my own thinking and efforts.
// Dylan Bish
// CMPSC 111 Spring 2017
// Lab 4
// Date:  9 February 2017
//
// Purpose: making some great art with Java! but this is the display
//==========================================

import javax.swing.*;

public class Lab4Display
{
  public static void main(String[] args)
  {
    JFrame window = new JFrame(" Dylan Bish ");

    // Add the drawing canvas and do necessary things to
    // make the window appear on the screen!
    window.getContentPane().add(new Lab4());
    window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    window.setVisible(true);
    window.setSize(600, 400);
  }
}

