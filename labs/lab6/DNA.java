//***************************
//Honor Code: The work that I am submitting is a result of my own thinking and efforts.
//Matthew Bissert and Dylan Bish
//CMPSC 111 Spring 2017
//Lab 6
//Date: 02 28 2017
//
//Purpose: Create a DNA manipulation program.
//***************************
import java.util.Date; // needed for printing today's date
import java.util.Scanner; 
import java.util.Random;


public class DNA
{
	public static void main (String[] args) 
	{


	//Variable Dictionary:
	//Declare your variables and use comments to explain their meanings

 System.out.println("Matthew Bissert & Dylan Bish\nLab 6 DNA Program\n"
            + new Date() + "\n----------------\n");


	String S, S1, S2, S3, S4, S5, DNAString;
        int len;       // length of dna string
        int location;  // one of the positions in the dna string
	char c;        // a letter chosen randomly from a string


	Scanner scan = new Scanner(System.in); 
	Random r = new Random(); // random number generator



	System.out.println("Please print out a single letter from a DNA sequence: A T G C");
		S1 = scan.nextLine(); //Original input 1
		S1 = S1.toLowerCase();

	System.out.println("Please print out a single letter from a DNA sequence: A T G C");
		S2 = scan.nextLine(); //Original input 2
		S2 = S2.toLowerCase();

	System.out.println("Please print out a single letter from a DNA sequence: A T G C");
		S3 = scan.nextLine(); //Original input 3
		S3 = S3.toLowerCase();


	System.out.println("Please print out a single letter from a DNA sequence: A T G C");
		S4 = scan.nextLine(); //Original input 4
		S4 = S4.toLowerCase();	
	

	S1 = S1.replace('a', 'T');
	S1 = S1.replace('t', 'A');
	S1 = S1.replace('c', 'G');
	S1 = S1.replace('g', 'C');

	S2 = S2.replace('a', 'T');
	S2 = S2.replace('t', 'A');
	S2 = S2.replace('c', 'G');
	S2 = S2.replace('g', 'C');

	S3 = S3.replace('a', 'T');
	S3 = S3.replace('t', 'A');
	S3 = S3.replace('c', 'G');
	S3 = S3.replace('g', 'C');

	S4 = S4.replace('a', 'T');
	S4 = S4.replace('t', 'A');
	S4 = S4.replace('c', 'G');
	S4 = S4.replace('g', 'C');

	DNAString = (S1 + S2 + S3 + S4);

	System.out.println("DNA Sequence:" + DNAString);
	
        c = DNAString.charAt(r.nextInt(5));

	System.out.println("Random letter added:" + DNAString + c);
	




	}
}


