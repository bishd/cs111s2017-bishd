//*************************************
// Honor Code: The work I am submitting is a result of my own thinking and efforts.
// Dylan Bish and Alana Picozzi
// CMPSC 111 Spring 2017
// Lab #8
// Date: March 9th 2017
//
// Purpose: makin that sweet sudoku checker
//*************************************
import java.util.Date; // needed for printing today's date
import java.util.Scanner;

public class SudokuCheckerMain

{

//----------------------------
// main method: program execution begins here
//----------------------------
public static void main(String[] args)

	{

// Label output with name and date:
System.out.println("Dylan Bish and Alana Picozzi \n Lab #8\n" + new Date() + "\n");
// Variable dictionary:
	SudokuChecker checker = new SudokuChecker();
	System.out.println("HOWDY DUDE. Welcome to the glorious 4 x 4 Sudoku checker!"); 
	checker.inputGrid();
	checker.checkGrid();
//this is the main class that calls the code from the other .java file, SudokuChecker.java

	}

}
