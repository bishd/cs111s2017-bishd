import java.util.Scanner;

public class SudokuChecker
//this is the main method
{

	private int w1, w2, w3, w4, x1, x2, x3, x4, y1, y2, y3, y4, z1, z2, z3, z4;

	public SudokuChecker()
{
w1 = 0;
w2 = 0; 
w3 = 0; 
w4 = 0;
x1 = 0; 
x2 = 0;
x3 = 0;
x4 = 0;
z1 = 0;
z2 = 0;
z3 = 0;
z4 = 0;
}
//this is the constructor for the rest of the program
public void inputGrid() //this is the part that takes the input from the user
	{


	Scanner scan = new Scanner(System.in);

  	System.out.println("Please enter the first row of integers, separating each individual integer with the enter key. ");
  	w1 = scan.nextInt();
	w2 = scan.nextInt();
	w3 = scan.nextInt();
	w4 = scan.nextInt();

	System.out.println(w1 + " " + w2 + " " + w3 + " " + w4);

  	System.out.println("Please enter the second row of integers, separating each individual integer with the enter key. ");
  	x1 = scan.nextInt();
	x2 = scan.nextInt();
	x3 = scan.nextInt();
	x4 = scan.nextInt();

	System.out.println(w1 + " " + w2 + " " + w3 + " " + w4);
	System.out.println(x1 + " " + x2 + " " + x3 + " " + x4);

  	System.out.println("Please enter the third row of integers, separating each individual integer with the enter key. ");
  	y1 = scan.nextInt();
	y2 = scan.nextInt();
	y3 = scan.nextInt();
	y4 = scan.nextInt();

	System.out.println(w1 + " " + w2 + " " + w3 + " " + w4);
	System.out.println(x1 + " " + x2 + " " + x3 + " " + x4);
	System.out.println(y1 + " " + y2 + " " + y3 + " " + y4);

  	System.out.println("Please enter the fourth row of integers, separating each individual integer with the enter key. ");
  	z1 = scan.nextInt();
	z2 = scan.nextInt();
	z3 = scan.nextInt();
	z4 = scan.nextInt();


	System.out.println(w1 + " " + w2 + " " + w3 + " " + w4);
	System.out.println(x1 + " " + x2 + " " + x3 + " " + x4);
	System.out.println(y1 + " " + y2 + " " + y3 + " " + y4);
	System.out.println(z1 + " " + z2 + " " + z3 + " " + z4);

//after it takes all the input, the code displays the input in a grid for the user to look at

	}

public void checkGrid() //this is the part that actually makes the grid!

	{

	int row1 = w1 + w2 + w3 + w4;
	int row2 = x1 + x2 + x3 + x4;
	int row3 = y1 + y2 + y3 + y4;
	int row4 = z1 + z2 + z3 + z4;

	int col1 = w1 + x1 + y1 + z1;
	int col2 = w2 + x2 + y2 + z2;
	int col3 = w3 + x3 + y3 + z3;
	int col4 = w3 + x3 + y3 + z3;


	int reg1 = w1 + w2 + x1 + x2;
	int reg2 = w3 + w4 + x3 + x4;
	int reg3 = y1 + y2 + z1 + z2;
	int reg4 = y3 + y4 + z3 + z4; 
//these are the variables so that we were able to check their values in each region, column and row


	if (row1 == 10) {
	System.out.println("ROW-1:GOOD ");
	} else {
	System.out.println("ROW-1:BAD ");
	}

	if (row2 == 10) {
	System.out.println("ROW-2:GOOD ");
	} else {
	System.out.println("ROW-2:BAD ");
	}

	if (row3 == 10) {
	System.out.println("ROW-3:GOOD ");
	} else {
	System.out.println("ROW-3:BAD ");
	}

	if (row4 == 10) {
	System.out.println("ROW-4:GOOD ");
	System.out.println();
	} else {
	System.out.println("ROW-4:BAD ");
	System.out.println();
	}

	if (col1 == 10) {
	System.out.println("COL-1:GOOD ");
	} else {
	System.out.println("COL-1:BAD ");
	}

	if (col2 == 10) {
	System.out.println("COL-2:GOOD ");
	} else {
	System.out.println("COL-2:BAD ");
	}

	if (col3 == 10) {
	System.out.println("COL-3:GOOD ");
	} else {
	System.out.println("COL-3:BAD ");
	}

	if (col4 == 10) {
	System.out.println("COL-4:GOOD ");
	System.out.println();
	} else {
	System.out.println("COL-4:BAD ");
	System.out.println();
	}

	if (reg1 == 10) {
	System.out.println("REG-1:GOOD ");
	} else {
	System.out.println("REG-1:BAD ");
	}

	if (reg2 == 10) {
	System.out.println("REG-2:GOOD ");
	} else {
	System.out.println("REG-2:BAD ");
	}

	if (reg3 == 10) {
	System.out.println("REG-3:GOOD ");
	} else {
	System.out.println("REG-3:BAD ");
	}

	if (reg4 == 10) {
	System.out.println("REG-4:GOOD ");
	System.out.println();
	} else {
	System.out.println("REG-4:BAD ");
	System.out.println();
	}

	if (row1 == 10 && row2 == 10 && row3 == 10 && row4 == 10 && col1 == 10 && col2 == 10 && col3 == 10 && col4 == 10 && reg1 == 10 && reg2 == 10 && reg3 == 10 && reg4 == 10) {
	System.out.println("SUDOKU GRID IS GOOD :D ");
	System.out.println();
	} else {
	System.out.println("SUDOKU GRID IS BAD D: ");
	System.out.println();
// the last part tells us whether it is good :D or bad D: after it checks if everything works out!
	}






	}

}
