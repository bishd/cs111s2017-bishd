//*************************
// Dylan Bish
// CMPSC 111
// 19 January 2017
// Lab 1

// Listing 1.1 from Lewis & Loftus, slightly modified
// Demonstrates the basic structure of a Java application
//*************************

import java.util.Date;

public class Lab1Part2
{
	public static void main(String[] args)
	{
		System.out.println("Dylan Bish " + new Date());
		System.out.println("Lab 1");
		System.out.println("Have you ever heard the tale of Darth Plagueis the wise?");
		System.out.println("I thought not.  It's not a story the Jedi would tell you.");
		System.out.println("It's a Sith legend.  Darth Plagueis was a Dark Lord of the Sith,");
		System.out.println("so powerful and so wise he could use the Force to influence the midichlorians to create life...");

		//-----
		//Prints words of wisdom
		//-----

	System.out.println("Advice from James Gosling, creator of Java:");
	System.out.println("Don't be intimidated--give it a try!");
	}
}

