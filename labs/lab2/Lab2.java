
//*************************************
// Honor Code: The work I am submitting is a result of my own thinking and efforts.
// Dylan Bish
// CMPSC 111 Spring 2017
// Lab 2
// Date: 26 January 2017
//
// Purpose: the purpose of this program will be to convert a value of US Dollars to 5 other specific currencies
//*************************************
import java.util.Date; // needed for printing today's date

public class Lab2 //replace xxx with actual file name
{
	//-------------
	//main method: program execution begins
	//-------------
	public static void main(String[] args)
	{
		//Label output with name and date:
		System.out.println("Dylan Bish\nLab 2\n" + new Date() + "\n");
		
		//Variable dictionary:
		int USD = 1; 	//the value of the US Dollar compared to... itself
		double CND = 1.31; //the value of the Canadian dollar compared to USD
		double VietDong = 22592.50; //the value of the Vietnamese Dong compared to USD
		int bank = 300;
		int cash = 70;
		int spent = 20; //above are values of money I wish I had
		int total;
		total = bank + cash;
		int totall;
		totall = bank + cash - spent;

		System.out.println("Number of Canadian Dollars for every dollar is " + CND);
		System.out.println("Number of Vietnamese Dongs for every dollar is " + VietDong);
		System.out.println("Number of dollars I have in cash and in bank " + total);
		System.out.println("Number of dollars I have after spendings today " + totall);
		
	}
}

