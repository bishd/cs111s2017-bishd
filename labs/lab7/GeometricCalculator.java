public class GeometricCalculator {

    public static double calculateSphereVolume(double radius) {	 //accepts a double that is passed and sets it to radius
        double volume;
        volume = (4.0/3) * (Math.PI) * radius * radius * radius; //performs calculation, modified division statement to allow
        return volume;						 //for correct calculation without rounding
    }	

    public static double calculateTriangleArea(double a, double b, double c) {	//accepts three double variables for calculations
        double area;
	double s;
	s = (a + b + c) / 2;
        area = Math.sqrt(s*(s-a)*(s-b)*(s-c));			//performs calculation for triangle area
        return area;
    }

    public static double calculateCylinderVolume(double radius, double height) { //accepts two doubles to be radius and height
        double volume;
        volume = (Math.PI) * radius * radius * height;		//performs calculation
        return volume;
    }

    public static double calculateSphereSurfacearea(double radius)	{ //accepts a double to be radius 
	double surfacearea;
	surfacearea = 4 * (Math.PI) * radius * radius;		//performs calculation
	return surfacearea;
    }

    public static double calculateCylinderSurfacearea(double radius, double height)	{ //accepts two doubles to be radius and height
	double surfacearea;
	surfacearea = (2 * (Math.PI) * radius * height) + (2 * (Math.PI) * radius * radius);
	return surfacearea;
    }
}
