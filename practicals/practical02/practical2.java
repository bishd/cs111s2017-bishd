
//*************************************
// Honor Code: The work I am submitting is a result of my own thinking and efforts.
// Dylan Bish
// CMPSC 111 Spring 2017
// Practical #2
// Date: 27 January 2017
//
// Purpose: make a sweet cool tall house with a garage with ascii art
//*************************************
import java.util.Date; // needed for printing today's date

public class practical2 //replace xxx with actual file name
{
	//-------------
	//main method: program execution begins
	//-------------
	public static void main(String[] args)
	{
		//Label output with name and date:
		System.out.println("Dylan Bish\npractical #2\n" + new Date() + "/n");
		System.out.println("   //\\\\");
		System.out.println("  //  \\\\");
		System.out.println(" //____\\\\");
		System.out.println(" |  _  |");
		System.out.println(" | |_| |");
		System.out.println(" |     |");
		System.out.println(" |_____|");
		System.out.println(" |  _  |");
		System.out.println(" | |_| |_________");
		System.out.println(" |  _  |  |\'\'|  |");
		System.out.println(" |_|_|_|__|\'\'|__|");
	}
}

