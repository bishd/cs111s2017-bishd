import java.util.Date; // needed for printing today's date
import java.util.Scanner; //the scanner you doofus!
public class Lab3Debugged
{
    //----------------------------
    // main method: program execution begins here
    //----------------------------
    public static void main(String[] args)
    {
       // Label output with name and date:
        System.out.println("Original Bug Maker: Zijun Zhou\nLab 3\n" + new Date() + "\n");
        System.out.println("Dylan Bish\nPractical 3\n" + new Date() + "\n");
       // Variable dictionary:(Declare variables and use comments to explain their meanings)
   
    String name;
    double bill;
    double tip;
    double percentage;
    double total;
    int people;
    double share;

    Scanner scan = new Scanner (System.in);

    System.out.println("Please enter your name:");
    name = scan.nextLine();
    System.out.println(name + ",welcome to the Tip Calculator!");
   
    System.out.println("Please enter the amount of your bill:");
    bill = scan.nextDouble();

    System.out.println("Please enter the percentage that you want to tip:");
    percentage = scan.nextDouble();

    tip = percentage/100 * bill;
    System.out.println("Your original bill was $" + bill);
    System.out.println("Your tip amount is $" + tip);
   
    total = tip + bill;
    System.out.println("Your total bill is $" + total);
   
    System.out.println("How many people will be splitting the bill?");
    people = scan.nextInt();

    share = total / people;
    System.out.println("Each person should pay $" + share);
    System.out.println("Have a nice day! Thank you for using our service. ");
   
    }
}
