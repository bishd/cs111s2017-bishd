
//*************************************
// Honor Code: The work I am submitting is a result of my own thinking and efforts.
// Dylan Bish
// CMPSC 111 Spring 2017
// Lab 3
// Date: 2 February 2017
//
// Purpose: Tips and bills calculator!
//*************************************
import java.util.Date; // needed for printing today's date

public class Lab3 //replace xxx with actual file name
{
	//-------------
	//main method: program execution begins
	//-------------
	public static void main(String[] args)
	{
		//Label output with name and date:
		System.out.println("Dylan Bish\nLab 3\n" + new Date() + "\n");
		
		//Variable dictionary: 
		double bill;
		double tip;
		String name;
		double percentage;
		double total;
		int people;
		double share;
		tip = percentage + bill;
		total = tip + bill;
		share = people / code;

		Scanner scan = new Scanner(System.in);
		
		System.out.print("Howdy partner, what is your name?: ");
		name = scan.nextInt();

		System.out.print("Nice to meet you " + name + ", what is your bill?: "
		bill = scan.nextDouble();
		
		System.out.print("How much are you trying to tip? Enter a value between 0 and 1: ");
		percentage = scan.nextDouble();
		
		System.out.println("Okay, here is the value of your tip: " + tip);

		System.out.println("Here is the total bill with the tip included: " + total);

		System.out.println("How many people are splitting this bill?: ");
		people = scan.nextInt();

		System.out.println("Okay, this is how much each person should pay: " + share);
		System.out.println("Thanks " + name + ", have a wonderful day! ");

	}
}

