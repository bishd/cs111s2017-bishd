
//*************************************
// Honor Code: The work I am submitting is a result of my own thinking and efforts.
// Dylan Bish
// CMPSC 111 Spring 2017
// Practical 5
// Date: 24 February 2017
//
// Purpose: ...
//*************************************
import java.util.Date; // needed for printing today's date
import java.util.Scanner;

public class MadLib //replace xxx with actual file name
{
	//-------------
	//main method: program execution begins
	//-------------
	public static void main(String[] args)
	{
		//Label output with name and date:
		System.out.println("Dylan Bish\nPractical 5\n" + new Date() + "/n");
		
		//Variable dictionary:
		String noun1;
		int amount1;
		int amount2;
		String adjective1;
		String verb1;
		String illness;

		Scanner scan = new Scanner(System.in);

		System.out.println("Alright lad, get ready for some dumb jokes - please enter a noun:");
		noun1 = scan.nextLine();

		System.out.println("Cool, now enter how many of those nouns are being joked about, an interger please:");
		amount1 = scan.nextInt();

		System.out.println("Even cooler, now enter a second number, an integer please:");
		amount2 = scan.nextInt();

		System.out.println("Alright, now enter an adjective to describe the noun:");
		adjective1 = scan.next();

		System.out.println("We almost done, just slap in a verb:");
		verb1 = scan.next();

		System.out.println("Okay, one more - Enter any noun, but an illness would give you maximum joke efficiency:");
		illness = scan.next();

		System.out.println("I literally do not think you are ready for this bad joke:");

		System.out.println("If a guy has " + amount1 + " " + adjective1 + " " + noun1 + "(s),");
		System.out.println("and he " + verb1 + " " + amount2 + ",");
		System.out.println("what does he have?");
		System.out.println(illness + ".");


		
	}
}

