// This example is derived from a Java source code snippet found at:
// http://pages.cs.wisc.edu/~hasti/cs368/JavaTutorial/NOTES/Exceptions.html

public class ExceptionExample {

  static void throwsExceptions(int k, int[] A, String S) {
    int j = 1 / k;
    int len = A.length + 1;
    char c;

    try {
      c = S.charAt(0);
      if (k == 10) j = A[3];
    }
    catch (ArrayIndexOutOfBoundsException ex) {
      System.out.println("Array error");
	ex.printStackTrace();
    }
    catch (ArithmeticException ex) {
      System.out.println("Arithmetic error");
	ex.printStackTrace();
    }
    catch (NullPointerException ex) {
      System.out.println("Null pointer");
	ex.printStackTrace();
    }
    finally {
      System.out.println("In the finally clause");
    }
    System.out.println("After the try block");
  }

  public static void main(String[] args) {
    int[] X = {0,1,2};
    // throwsExceptions(0, X, "hi"); this causes the program to crash since you cannot divide by zero
    // throwsExceptions(10, X, ""); this causes the program to crash because array to be "out of bounds" since you must have some sort of string as the third value
    // throwsExceptions(10, X, "bye"); this will not cause the program to crash but it will print out the corresponding catch, which was an array error
    // throwsExceptions(10, X, null); this does the same as above, but instead prints out the null pointer catch, because it uses null as its third value
     throwsExceptions(15, X, "a wonderful day"); // this prints out none of the catches, but only the clause

  }

}
