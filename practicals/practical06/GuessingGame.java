
//*************************************
// Honor Code: The work I am submitting is a result of my own thinking and efforts.
// Dylan Bish
// CMPSC 111 Spring 2017
// Practical 6
// Date: 31 March 2017
//
// Purpose: Making a fun number guessing game!
//*************************************
import java.util.Date; // needed for printing today's date
import java.util.Random;
import java.util.Scanner;

public class GuessingGame //replace xxx with actual file name
{
	//-------------
	//main method: program execution begins
	//-------------
	public static void main(String[] args)


	{
		//Label output with name and date:
		System.out.println("Dylan Bish\nLab #\n" + new Date() + "/n");
		
		//Variable dictionary:
		Random generator = new Random();
		int random;
		int input;
		int tries = 0;

		random = generator.nextInt(100) + 1; //the constructor which assigns the value, +1 needed so it's not 0

		Scanner scan = new Scanner(System.in);

		System.out.println("Welcome to the Guessing Game! A random integer between 1 and 100 has been generated, try and guess the number!");

		input = scan.nextInt();
		tries++;

		while (random != input)
		{
			
		if (input > random)
		{
			System.out.println("Try again buddy, too high!");
			input = scan.nextInt();
			tries++;
		}
		else if (input < random)
		{
			System.out.println("Try again buddy, too low!");
			input = scan.nextInt();
			tries++;
		}
		else
		{
			System.out.println("Good job! The number is " + random);
		}

		
		}

		System.out.println("After " + tries + " guesses, the numbers were the same! The number was " + random + "."); //this is placed after the while loop when the program finishes

	}
}

